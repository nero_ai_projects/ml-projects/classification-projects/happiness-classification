from happiness.main import HappinessPredictor


# Пример использования
predictor = HappinessPredictor()

# Пример входных данных
input_data = {
    'infoavail': 4,
    'housecost': 3,
    'schoolquality': 5,
    'policetrust': 2,
    'streetquality': 4,
    'events': 3
}

# Сделать предсказание
happiness_status = predictor.predict(input_data)

# Вывод результата
print("Happiness Status:", "Happy" if happiness_status == 1 else "Unhappy")
