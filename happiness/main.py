import joblib
import pandas as pd
import os


class HappinessPredictor:
    def __init__(self):
        curr_path = os.path.dirname(__file__)
        # Загрузка предварительно обученной модели и scaler
        self.model = joblib.load(f'{curr_path}/src/model.pkl')
        self.scaler = joblib.load(f'{curr_path}/src/scaler.pkl')

    def preprocess(self, input_data):
        # Преобразование входных данных в DataFrame
        data_df = pd.DataFrame([input_data])
        
        # Нормализация данных с использованием scaler
        scaled_data = self.scaler.transform(data_df)
        return scaled_data

    def predict(self, input_data):
        # Предобработка входных данных
        processed_data = self.preprocess(input_data)

        # Предсказание с помощью модели
        prediction = self.model.predict(processed_data)
        return prediction[0]
